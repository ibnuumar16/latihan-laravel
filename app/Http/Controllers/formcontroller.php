<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class formcontroller extends Controller
{
    public function from(){
        return view('latihan.from');
    }

    public function kirim(Request $request){
        $nama = $request['nama'];
        return view('latihan.selamatdatang', compact('nama'));
    }   
}

